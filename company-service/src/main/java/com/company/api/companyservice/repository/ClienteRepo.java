package com.company.api.companyservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.company.api.companyservice.entity.Cliente;

public interface ClienteRepo extends CrudRepository<Cliente, Long> {

}
