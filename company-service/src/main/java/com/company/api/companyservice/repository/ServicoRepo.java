package com.company.api.companyservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.company.api.companyservice.entity.Servico;

@RepositoryRestResource(path = "servicos", collectionResourceRel = "servicos")
public interface ServicoRepo extends CrudRepository<Servico, Long> {

}
