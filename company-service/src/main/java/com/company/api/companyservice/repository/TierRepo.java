package com.company.api.companyservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.company.api.companyservice.entity.Tier;

public interface TierRepo extends CrudRepository<Tier, Long> {

}
