package com.company.api.companyservice;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.company.api.companyservice.entity.Servico;
import com.company.api.companyservice.entity.Tier;
import com.company.api.companyservice.repository.ClienteRepo;
import com.company.api.companyservice.repository.ServicoRepo;
import com.company.api.companyservice.repository.TierRepo;

@Component
public class AppStartupRunner implements ApplicationRunner {
	@Autowired
	private ClienteRepo clienteRepo;

	@Autowired
	private ServicoRepo servicoRepo;

	@Autowired
	private TierRepo tierRepo;

	@Override
	public void run(ApplicationArguments args) throws Exception {

		if (tierRepo.count() == 0) {
			Tier ouro = new Tier("Ouro", (double) 10, null, true);
			Tier prata = new Tier("Prata", (double) 5, null, true);

			tierRepo.save(ouro);
			tierRepo.save(prata);
		}

		if (clienteRepo.count() == 0) {
			Optional<Tier> findById = tierRepo.findById((long) 1);
			Tier tier = findById.isPresent() ? findById.get() : null;

//			Cliente cliente = new Cliente("Rodrigo Silva", "rodrigo.silva@lecom.com.br", "123456789", tier, null, true);

//			clienteRepo.save(cliente);
		}

		if (servicoRepo.count() == 0) {
			Servico servico = new Servico("Serviço testes", (double) 300, 5, (double) 5, null, true);

			servicoRepo.save(servico);
		}

	}

}
