package com.company.api.companyservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.company.api.companyservice.entity.Contrato;

@RepositoryRestResource(path = "contratos", collectionResourceRel = "contratos")
public interface ContratoRepo extends CrudRepository<Contrato, Long> {

}
