package com.company.api.companyui.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cliente {
	private String nomeCliente;
	private String emailCliente;
	private String telefone;
//	private Tier tier;
//	private List<Contrato> contratos;
	private Boolean ativo;
}
