package com.company.api.companyui.controller;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.company.api.companyui.entity.Cliente;

@Controller
@RequestMapping("/")
public class CompanyController {
	private final RestTemplate restTemplate;

	public CompanyController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@RequestMapping("")
	public ModelAndView index() {
		return new ModelAndView("index");
	}

	@GetMapping("/clientes")
	public ResponseEntity<List<Cliente>> listClasses() {
		return restTemplate.exchange("http://company-service:8081/api/clientes", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Cliente>>() {
				});
	}
}
